## Api REST
### Proyecto Modulo III Diplomado FullStack
Documentación necesaria para utilizar los servicios de API.

# API
## MODULO III - Gestión de usuario

Registrar datos de usuario mediante el `nombreCompleto, edad`.

**Método** : `POST`

Obtener datos de usuario mediante el `id, nombreCompleto, edad`.

**Método** : `GET`

Actualizar datos de usuario mediante el `id`.

**Método** : `PUT`

Eliminar usuario mediante el `id`.

**Método** : `DELETE`

Obtener promedio de edad de usuarios mediante el `/usuarios/promedio-edad`.

**Método** : `GET`

Obtener estado de la API mediante el `/status`.

**Método** : `GET`









```
/api/modulo3/usuario/:ci
```

Parámetros de entrada:

Seccess 200:

Campo | Tipo | Descripción
--- | --- | ---
**id** | `integer` | *Número único asignado al usuario en la base de datos.*
**name** | `string` | *nombre de usuario.*
**last_name** | `string` | *apellido.*
**edad** | `integer` | *edad.*

Error 5xx:

### Instalacion

crear un entorno virtual de desarrollo en la misma carpeta del clonado del proyecto con el siguiente comando
**python3 -m venv env**

activar el entorno virtual bajo el siguiente comando
Windows
```
\env\Scripts\activate
```
Linux o Mac
```
. env/bin/activate
```

Instalar los requerimientos del proyecto bajo el entorno de desarrollo 

```
pip install -r requeriments.txt
```

Ingresar a la carpeta del proyecto "api_diplomado"

```
cd api_diplomado
```

Ejecutar el proyecto

```
python3 manage.py runserver
```