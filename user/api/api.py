from multiprocessing import context
from urllib import request
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from user.models import User
from user.api.serializers import UserListSerializer, UserSerializer

@api_view(['GET', 'POST'])
def UserApiViewFunction(request):
    # Listar usuarios
    if request.method == 'GET':
        users = User.objects.all().values('id', 'name', 'last_name', 'edad')
        users_serializer = UserListSerializer(users, many = True)
        return Response(users_serializer.data, status = status.HTTP_200_OK)
    # crear usuario
    elif request.method == 'POST':
        users_serializer = UserSerializer(data = request.data)
        if users_serializer.is_valid():
            users_serializer.save()
            return Response({ 'message': 'Usuario creado correctamente'}, status=status.HTTP_200_OK)
            
            #return Response(users_serializer.data, status= status.HTTP_201_CREATED)
        return Response(users_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def UserApiDetailApiView(request, pk= None):
    # Consulta
    user = User.objects.filter(id = pk).first()

    # Validación
    if user:
        # Retrieve
        if request.method == 'GET':
            user_serializer = UserSerializer(user)
            return Response(user_serializer.data, status = status.HTTP_200_OK)
        # Actualización
        elif request.method == 'PUT':
            user_serializer = UserSerializer(user, data=request.data)
            if user_serializer.is_valid():
                user_serializer.save()
                return Response(user_serializer.data, status = status.HTTP_200_OK)
            return Response(user_serializer.errors, status = status.HTTP_400_BAD_REQUEST)
        # Eliminar
        elif request.method == 'DELETE':
            user.delete()
            return Response({ 'message': 'Usuario Eliminado correctamente'}, status=status.HTTP_200_OK)
    return Response({'message': 'No se encontro un usuario'}, status=status.HTTP_400_BAD_REQUEST)
