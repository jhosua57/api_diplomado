from django.urls import path
# from apps.users.api.api import UserAPIView
from user.api.api import UserApiViewFunction, UserApiDetailApiView

urlpatterns = [
    path('usuario/', UserApiViewFunction, name = 'usuario_api'),
    path('usuario/<int:pk>/', UserApiDetailApiView, name = 'usuario_detail')

]
