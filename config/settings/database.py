#import os
from pathlib import Path

# Build paths inside the project like this: BASE_DIR / 'subdir'.
#BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
BASE_DIR = Path(__file__).resolve().parent.parent
# Database
# https://docs.djangoproject.com/en/4.0/ref/settings/#databases

SQLITE = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}

POSTGRESQL_DEV ={
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'modulo3_db',
        'USER': 'modulo3_db_user',
        'PASSWORD': 'yzwzCIbgBLpRIQHg3KHV0vuqnxloQmPp',
        'HOST': 'dpg-cf3elm1gp3jl0q45t6ig-a.oregon-postgres.render.com',
        'PORT': '5432'
    }
}
